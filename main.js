const express = require("express");
const { subnetMatch } = require("ipaddr.js");
const app = express();
const PORT = 3000;

console.log("Server-side program starting");



app.get('/', (req, res) => {
    res.send('Hello world');
});

/**
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
const add = (a, b) => {
    const sum = a+b;
    return sum;
};

app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(req.query.a, req.query.b);
    res.send(sum);
})

app.listen(PORT, () => console.log(`Server listening http://localhost:${PORT}`));

