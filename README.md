# Se oli sitten siinä

Kukaan ei oikeastaan tiedä, miksi Maapallo yhtäkkiä räjähti.

## Todistajien lausunto

1. Minä
2. Hän
3. Tuo
4. Me

**1.**

>Se ei ollut kuin olisi voinut olettaa. Ottaen huomioon sen tosiseikan, että planeetta kuitenkin räjähti. Uskaltaisin väittää ettei aluksemme edes liikkunut tapahtuman aikana, vaikka useista ikkunoidemme ohi leijuvista partikkeleista voisi päätellä, että rähäjdyksestä syntyi edes jonkin verran liike-energiaa. Tämän enempää en voi asiasta itse sanoa, koska olin vessassa varsinaisen tapahtuman aikana.

**2.**

>Uskon olevani sokea. Viimeinen asia jonka näin oli kirkas valkoinen valo siinä kohdassa, jossa Maa oli ollut. Se näytti hieman silmältä.

**3.**

>Tuo ei pysty tällä hetkellä kirjoittamaan, joten kirjoitan hänen analyysinsä hänen puolestaan. 
>"AAAAAAAAAAAAAÄAÄAÄAÄÄAÄÄÄÄÄÄÄÄÄAÄAAAAAAAAA!"

**4.**

>Jos me jostain olemme yhtä mieltä, niin siitä, että tulemme kaikki todennäköisesti kuolemaan seuraavan kolmen viikon aikana.

>>`del.ht => (visit, G)={'/dead', (name, year, location(0,0,0))};`  
>>`foreach (del.ht H in f+r){`  
>>>`fill(location);`  
>>>`this.name = name;`  
>>>`this.year = gg://base/catalog/calendar/year/current;`  
>>>`send.this;`  
`}`